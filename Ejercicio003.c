#include <stdio.h> //Librería estandard input output

//Punto de entrada de nuestro programa
int main()
{
    printf("Dame un numero\n");
    
    int a;
    
    scanf("%d",&a);       
    getchar();
    
    printf("Dame otro numero\n");
    
    int b;
    
    scanf("%d",&b);       
    getchar();
    
    // Determinar cual es el mayor
    if(b>a){
        printf("B es el mayor %d>%d\n",b,a);
    }else if(a>b){
        printf("A es el mayor %d>%d\n",b,a);
    }else if(a==b){
        printf("A es igual a B %d==%d\n",b,a);
    }
       
    getchar();
    
    return 0;
}