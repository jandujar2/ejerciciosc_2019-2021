#include "raylib.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1280;
    const int screenHeight = 720;
    const int velocidady = 8;
    const int ballSize = 25;
    
   
   
    Rectangle paladerecha;
    
    paladerecha.width = 20;
    paladerecha.height = 100;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
    
    
    Rectangle palaizquierda;
    
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    Vector2 ballVelocity;
    ballVelocity.x = 8;
    ballVelocity.y = 8;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // Gestiono pulsaciones botones
        if (IsKeyDown(KEY_Q)){
          palaizquierda.y -= velocidady;
        }
        
        if (IsKeyDown(KEY_A)){
          palaizquierda.y += velocidady;
        }
        
        if (IsKeyDown(KEY_UP)){
          paladerecha.y -= velocidady;
        }
        
        if (IsKeyDown(KEY_DOWN)){
          paladerecha.y += velocidady;
        }
        
        //Consulto Limites
        if(palaizquierda.y<0){
            palaizquierda.y = 0;
        }
        
        if(palaizquierda.y > (screenHeight - palaizquierda.height)){
            palaizquierda.y = screenHeight - palaizquierda.height;
        }
        
        if(paladerecha.y<0){
            paladerecha.y = 0;
        }
        
        if(paladerecha.y > (screenHeight - paladerecha.height)){
            paladerecha.y = screenHeight - paladerecha.height;
        }
        
        //Gestionamos el movimiento de la Bola
        ball.x += ballVelocity.x;
        ball.y += ballVelocity.y;
        
        if((ball.x > screenWidth - ballSize) || (ball.x < ballSize) ){
            ballVelocity.x *=-1;
        }
        
        if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
            ballVelocity.y *=-1;
        }

        
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(BLUE);

            // Pinto Pala Derecha
            DrawRectangleRec(paladerecha, GREEN);
            
            // Pinto Pala Izquierda
            DrawRectangleRec(palaizquierda, GREEN);
            
            // Pintamos la pelota
            DrawCircleV(ball, ballSize, GREEN);  
            
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}