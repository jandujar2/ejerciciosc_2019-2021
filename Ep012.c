//12. Write a program that asks for 10 numbers to the user and show the sum and product of all of them. Use while loop.

#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main(){
    
    float num[9]; // Declarar un array de numeros decimales
    
    int i = 0;
    while(i<10){
        printf("Dame un numero: ");
        scanf("%f",&num[i]);
        getchar();
        
        //i = i + 1; 
        i++;        
    }
    
    float suma = 0; // Declarar la variable decimal "suma"
    float mult = 1; // Declarar la variable decimal "mult"
    
    for (int j = 0; j < 10; j++) // Sumar y multiplicar todos los numeros del array, mediante un bucle
    {
        suma += num[j];
        mult *= num[j];
    }
    
    printf("\nLa suma total es: "); // Mostrar en la pantalla lo que esta escrito entre " "
    printf("%.3f", suma); // Mostrar la suma de los numeros del array

    printf("\nLa multiplicacion total es: "); // Mostrar en la pantalla lo que esta escrito entre " "
    printf("%.3f", mult); // Mostrar la multiplicacion de los numeros del array
       
    getchar();
    return (0); // Devolver 0. (Final del programa)
}