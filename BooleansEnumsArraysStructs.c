#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int main()
{
    char a = 'a';
    char nombre[] = "Enrique";
    int i = 0;
    float f = 12.f;
    double d = 3.141592;
    
    bool b = true;  // o false
    
    
    char tecla = getch();
    bool teclaAPulsada = false;
    //int teclaAPulsadaInt = 0;
    
    typedef int DiaDeLaSemana;
    enum
    {
       Lunes,       // = 0
       Martes,      // = 1
       Miercoles,   // = 2
       Jueves,
       Viernes,
       Sabado,
       Domingo,     // = 6
    };
    
    typedef int MesesDelAnyo;
    enum
    {
        Enero,
        Febrero,
        Marzo, 
        Abril,
        Mayo,
        Junio,
        Julio,
        Agosto,
        Septiembre,
        Octubre,
        Noviembre,
        Diciembre,
    };
    
    MesesDelAnyo navidad = Diciembre;
    
    // if (tecla == 'A')
    // {
        // teclaAPulsada = true;
    // }
    // else
    // {
        // teclaAPulsada = false;
    // }
    
    teclaAPulsada = tecla == 'A';

    printf("La tecla A ");
    if (teclaAPulsada)
    {
        printf("si ");
    }
    else
    {
        printf("no ");
    }
    printf("ha sido pulsada.\n");

    DiaDeLaSemana hoy = Lunes;
    DiaDeLaSemana manyana = Martes;
    
    if (hoy == Lunes)
    {
        printf("Estoy de mal humor.\n");
    }
    else
    {
        printf("Estoy neutral.\n");
    }
    
    printf("Hoy es");
    hoy = 25;
    
    switch (hoy)
    {
        case Lunes: 
        case Martes:
        case Miercoles:
        case Jueves:
        case Viernes:
            printf("Que fastidio, toca trabajar\n");
            break;
        case Sabado:
        case Domingo:
            printf("Me quedo en casa a rascarme la nariz\n");
            break;
        default:
            printf("Enrique, eres un graciosete, te metes las gracias por...\n");
            break;
    }
    
    switch(hoy)
    {
        case Sabado:
        case Domingo:
            printf("Hoy es finde");
            break;
        default:
            printf("Hoy NO es finde.");
            break;
    }
    
    
    getch();

    
    return 0;
}