#include <stdio.h>

//16. Write a program that that displays the first 100 prime numbers

int main ()
{
   int num=2;
   int prime;
   
   printf("The first 100000 prime numbers are:\n2, ");
   
    while (num < 100000)
    {
        
        num++;
        //reset and declaration of variable prime
        prime=1;
            
        //we have one number and we loop the divider while obtaining the remainder so we can check if the number is prime
        for(int x=2; x < num; x++)
        {
            
            if(num % x == 0)
            {
                prime = 0;
                break;
            }
               
        }
         //if prime still equals one after the previous loop, that means the number is prime and will be printed and the counter will go up by one
        if (prime == 1)
        {
            printf("%d, ", num);
        } 
    }
    
    getchar ();
    
    return 0;
}