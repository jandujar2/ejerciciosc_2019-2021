#include <stdio.h>
#include <stdlib.h>

int CalculaFactorial(int numero){
    int salida = 1;
    printf("El factorial de %d = ",numero);
    for(int i=numero;i>0;i--){
        printf("%d x",i);
        salida = salida * i;
    }
    
    printf("\b\b %d\n",salida);
    return salida;
}

int main () {

   int tmp;
   int factorial;
   
   
   printf("Dame 1 numero\n");
   scanf("%d",&tmp);
   getchar();
   
   factorial = CalculaFactorial(tmp);
   
   printf("El factorial de %d es %d\n",tmp,factorial);

   getchar();
   return(0);
}