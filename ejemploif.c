/*
  Ejemplo IF:
    if (condición) { sentencia; }  ← MEJOR ESTA FORMA
	o
	if (condición)
	{
		// Aquí se incluye el código 
        // que ha de ejecutarse sólo 
        // si se cumple la condición del if
	}

  
*/

#include <stdlib.h>
#include <stdio.h>

int main(){
    int a = 3, b = 4;

    if (( a == b ) || ( a == 3 )){
        printf("A es igual a B\n");
    }

    if ( a != b ){
        printf("A es diferente a B\n");
    }
    
    if ( a > b ){
        printf("A es mayor que B\n");
    }
    
    if ( a < b ){
        printf("A es menor que B\n");
    }
    
    if ( a >= b ){
        printf("A es mayor o igual a B\n");
    }
    
    if (( a <= b ) && (a == 4)) {
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
        printf("A es menor o igual a B\n");
    }
 
    if ( a==b ){
        printf("A es igual a B\n");
    }else if(a == 4){
        printf("A es 3\n");
    }else if ( b == 4){
        printf("B es 4\n");
    }else{
        printf("A es diferente a B\n");
    }
    
    
    if(a>b){
        printf("A>B");
    }else if(a == b){
        printf("A==B");
    }else{
        if(a==3){
            printf("A es 3");
        }else{
            printf("A no es 3");
        }
    }
 
    getchar();
    
    return 0;
}