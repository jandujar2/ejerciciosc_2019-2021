/*******************************************************************************************
*
*   raylib example - Basic Game Structure
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013 Ramon Santamaria (Ray San - raysan@raysanweb.com)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, END } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
	char windowTitle[30] = "Final PONG";
    
    GameScreen screen = LOGO;
	
	int framesCounter = 0;
	int timeCounter = 10;
	
	Color logoColor = BROWN;
	logoColor.a = 0;

    InitWindow(screenWidth, screenHeight, windowTitle);
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen)
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
				framesCounter++;
				
				if (logoColor.a < 255) logoColor.a++;
				
				if (framesCounter > 300)
				{
					screen = TITLE;
					framesCounter = 0;
				}

            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
				if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
				
				framesCounter++;

            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
				framesCounter++;
				
				if (framesCounter == 60)
				{
					timeCounter--;
					framesCounter = 0;
				}
				
				if (timeCounter < 0) screen = END;

            } break;
            case END: 
            {
                // Update END screen data here!
				if (IsKeyPressed(KEY_ENTER))
				{
					screen = TITLE;
					timeCounter = 10;
				}

            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
					DrawText("LOGOOOO", 200, 200, 80, logoColor);
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
					DrawText("FINAL PONG", 100, 100, 100, BEIGE);
					
					if ((framesCounter/30)%2) DrawText("PRESS ENTER", 300, 300, 30, BLUE);
					
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
					DrawText("this is the gameplay of final pong...", 150, 200, 20, RED);
					DrawText(FormatText("%i", timeCounter), 350, 50, 40, MAROON);
					
                } break;
                case END: 
                {
                    // Draw END screen here!
					DrawText("YOU LOOOSE... this is the end...", 100, 200, 30, DARKGREEN);

                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}