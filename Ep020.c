#include <stdio.h>
#include <stdlib.h>

/*20. Write a calculator program. Program flow should be as follow:
1) Ask for a number (float)
2) Ask for desired operation: '+', '-', '*', '/'
3) Ask for another number (float)
4) Show operation and results
5) Go to step 2)
*/

typedef enum OPERACION{
    SUMA,
    RESTA,
    MULTIPLICACION,
    DIVISION,
    SALIR,
    ERROR
}toperaciondef;

int main ()
{
    toperaciondef toperacion = ERROR;
    float num1;
    float num2;
    char coperation;
    
    while (toperacion != SALIR)
    {
        printf("Type a number\n");
        scanf("%f", &num1);
        getchar();
        
        system("cls");
        
        //operation selection
        printf("Desired operation\n\n");
        printf("+: Sum +\n");
        printf("-: Subtraction -\n");
        printf("*: Multiplication *\n");
        printf("/: Division /\n");
        printf("q: Quit /\n");
        
        scanf("%c", &coperation);
        getchar();
        
        switch(coperation){
            case '+': 
                toperacion = SUMA;
                break;
            case '-':
                toperacion = RESTA;
                break;
            case '*':
                toperacion = MULTIPLICACION;
                break;
            case '/':
                toperacion = DIVISION;
                break;
            case 'q':
                toperacion = SALIR;
                break;
            default:
                printf("\nError, wrong Operation\n");
                printf("Try again, please\n");
                toperacion = ERROR;
        }
        
        if(toperacion != ERROR){
        
            system("cls");
                   
            printf("Type another number\n");
            scanf("%f", &num2);
            getchar();
            
            system("cls");
            
            //operation result
            switch(toperacion){
                case SUMA:
                    printf("%f + %f = %f", num1, num2, num1+num2);
                    break;
                case RESTA:
                    printf("%f - %f = %f", num1, num2, num1-num2);
                    break;
                case MULTIPLICACION:
                    printf("%f * %f = %f", num1, num2, num1*num2);
                    break;
                case DIVISION:
                    printf("%f / %f = %f", num1, num2, num1/num2);
                    break;
                case SALIR:
                    printf("Adios!");
                    break;
                default:
                    printf("WTF");
                    break;
            }
            
            getchar();
                
            system("cls");
        }
    }    
    
        
        
        return 0;
       
        
}