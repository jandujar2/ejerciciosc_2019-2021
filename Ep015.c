#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    int num; // Declarar la variable entera "num"
    int residuo; // Declarar la variable entera "residuo"
    int n[50]; // Declarar un array de 50 numeros decimales, ya que no sabemos cuantos bits ocupa el numero que introduce el usuario
    int cont = 0; // Declarar la variable entera "cont" y determinarle el valor 0
    
    printf("Introduce un numero entero: "); // Pedir al usuario que introduzca un numero
    scanf("%d",&num); // Determinar el valor introducido a la variable "num"
    getchar();
    
    while( num <= 0) // Si el numero introducido es negativo o igual a 0, volver a pedir un valor hasta que este sea positivo y determinarle a "num"
    {
        printf("Introduzca un numero entero positivo: ");
        
        scanf("%d",&num);
        getchar();
    }
    
    while( num > 0) // Funcion para determinar cada bit en orden invertido en el array
    {
        residuo = num % 2;
    
        num /= 2;
        
        n[cont] = residuo;
        cont ++;
    }
    
    for( int i = cont-1; i >= 0; i --) // Funcion para leer el array en orden invertido, mostrando asi, la posicion correcta de los bits
    {
        printf("%d", n[i]);
    }
    
    getchar();
    return (0); // Devolver 0. (Final del programa)
}