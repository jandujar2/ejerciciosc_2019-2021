#include <stdio.h>
#include "jandu.h"

//18. Write a function to check if an integer is negative. Function declaration should be: int IsNegative(int num);

int main()
{
    int numMain;
    int call;
    
    printf("Please, type an integer\n");
    scanf("%i", &numMain);
    getchar();
    
    call = IsNegative (numMain);
       
    if (call == 1)
    {
        printf("The number is negative");
    }
    
    else
    {
        printf("The number is positive");
    }
        
    getchar();
}



